# README #

KUMu ASSESMENT TEST

### PODS USED? ###

* Alamofire - for Api calling.
* SwiftyJSON - read and process JSON data from an API/Server.
* MSPeekCollectionViewDelegateImplementation
* Reusable
* Kingfisher - downloading and caching images from the web.


### Architecture Used  ###

* I Used MVVM to reduce the amount of business logic in code-behind amd It sometimes overlaps
* with maintainability, because of the clean separation boundaries and more granular pieces of code. The 
* only disadvantages of it simple UI's can be an overkill

### Data Persistence ###

* I Used NSObject and NSCODING so you can easily store data and call the data effortlessly


